﻿using Newtonsoft.Json.Linq;
using System;
using System.Windows.Forms;

namespace JsonToClass
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            textBoxJson.SelectionStart = 0;
        }

        private void ButtonFormat_Click(object sender, EventArgs e)
        {
            try
            {
                textStatus.Text = "";
                var json = JObject.Parse(textBoxJson.Text);
                textBoxJson.Text = json.ToString();
            }
            catch (Exception ex)
            {
                textStatus.Text = ex.Message;
            }
        }

        private void ButtonConvert_Click(object sender, EventArgs e)
        {
            try
            {
                textStatus.Text = "";
                var conv = new JsonClassConv();
                textBoxClass.Text = conv.Convert(textBoxJson.Text);
            }
            catch (Exception ex)
            {
                textStatus.Text = ex.Message;
            }
        }
    }
}
