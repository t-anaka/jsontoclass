﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace JsonToClass
{
    public class JsonClassConv
    {
        private class PropData
        {
            public string Type { get; set; }
            public string Array { get; set; }
            public string Name { get; set; }
        }

        private class ClassData
        {
            public string Path { get; set; }
            public string Name { get; set; }
            public List<PropData> Prop { get; set; }
        }

        List<JToken> tokenDatas = new List<JToken>();
        List<ClassData> classDatas = new List<ClassData>();

        public string Convert(string json)
        {
            json = json.Trim();
            if (json.Length == 0)
                return "";
            if (json[0] != '{')
                return "";
            tokenDatas.Clear();
            classDatas.Clear();
            SetTokenData(JObject.Parse(json));
            SetClassData();
            MargeClassData();
            RenameClassData();
            return CreateClass();
        }

        private void SetTokenData(JToken token)
        {
            if (token.Type == JTokenType.Object)
                tokenDatas.Add(token);
            foreach (JToken child in token.Children())
                SetTokenData(child);
        }

        private string GetPath(string path)
        {
            path = Regex.Replace(path, "\\[.*?\\]", "", RegexOptions.Singleline);
            if (path == "")
                path = "root";
            else
                path = $"root.{path}";
            return path;
        }

        private string GetName(string path)
        {
            var split = path.Split('.');
            var name = split[split.Length - 1];
            int dummy;
            if (int.TryParse($"{name[0]}", out dummy))
                name = $"_{name}";
            return name;
        }

        public string ToSnake(string name)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < name.Length; i++)
            {
                if (i == 0)
                    sb.Append($"{name[0]}".ToLower());
                else if (char.IsUpper(name[i]))
                {
                    if (!char.IsUpper(name[i - 1]))
                    {
                        if (name[i - 1] != '_')
                            sb.Append("_");
                    }
                    sb.Append($"{name[i]}".ToLower());
                }
                else
                    sb.Append($"{name[i]}");
            }
            return sb.ToString();
        }

        private string ToUpCamel(string name)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < name.Length; i++)
            {
                if (i == 0)
                    sb.Append($"{name[0]}".ToUpper());
                else if (name[i - 1] == '_' && name[i] != '_')
                {
                    sb.Remove(i - 1, 1);
                    sb.Append($"{name[i]}".ToUpper());
                }
                else
                    sb.Append($"{name[i]}");
            }
            return sb.ToString();
        }

        private string ToLowCamel(string name)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < name.Length; i++)
            {
                if (i == 0)
                    sb.Append($"{name[0]}".ToLower());
                else if (name[i - 1] == '_' && name[i] != '_')
                {
                    sb.Remove(i - 1, 1);
                    sb.Append($"{name[i]}".ToUpper());
                }
                else
                    sb.Append($"{name[i]}");
            }
            return sb.ToString();
        }

        private void SetClassData()
        {
            foreach (var token in tokenDatas)
            {
                var path = GetPath(token.Path);
                var data = new ClassData();
                var name = GetName(path);
                data.Path = path;
                data.Name = ToUpCamel(name);
                data.Prop = new List<PropData>();
                foreach (var child in token.Children())
                {
                    if (child.Type == JTokenType.Property)
                    {
                        var prop = GetPropData((JProperty)child);
                        data.Prop.Add(prop);
                    }
                }
                classDatas.Add(data);
            }
        }

        private PropData GetPropData(JProperty prop)
        {
            var data = new PropData();
            var name = GetName(prop.Name);
            data.Name = ToSnake(name);
            data.Type = "object";
            data.Array = "";
            switch (prop.Value.Type)
            {
                case JTokenType.String:
                    data.Type = "string";
                    break;
                case JTokenType.Integer:
                    data.Type = "int";
                    break;
                case JTokenType.Float:
                    data.Type = "float";
                    break;
                case JTokenType.Boolean:
                    data.Type = "bool";
                    break;
                case JTokenType.Bytes:
                case JTokenType.Raw:
                    data.Type = "byte[]";
                    break;
                case JTokenType.Date:
                    data.Type = "DateTime";
                    break;
                case JTokenType.TimeSpan:
                    data.Type = "TimeSpan";
                    break;
                case JTokenType.Guid:
                    data.Type = "Guid";
                    break;
                case JTokenType.Uri:
                    data.Type = "Uri";
                    break;
                case JTokenType.Object:
                    data.Type = ToUpCamel(name);
                    break;
                case JTokenType.Array:
                    return GetArrayPropData(name, prop);
                default:
                    break;
            }
            return data;
        }

        private PropData GetArrayPropData(string name, JProperty prop)
        {
            var data = new PropData();
            data.Name = $"{ToSnake(name)}";
            data.Type = "object";
            var sb = new StringBuilder("[]");
            var token = prop.Value.First;
            if (token == null)
            {
                data.Array = sb.ToString();
                return data;
            }
            while (token.Type == JTokenType.Array)
            {
                sb.Append("[]");
                var childs = token.Children().ToArray();
                if (childs.Length == 0)
                {
                    data.Array = sb.ToString();
                    return data;
                }
                else
                    token = childs[0];
            }
            data.Array = sb.ToString();
            switch (token.Type)
            {
                case JTokenType.String:
                    data.Type = "string";
                    break;
                case JTokenType.Integer:
                    data.Type = "int";
                    break;
                case JTokenType.Float:
                    data.Type = "float";
                    break;
                case JTokenType.Boolean:
                    data.Type = "bool";
                    break;
                case JTokenType.Date:
                    data.Type = "DateTime";
                    break;
                case JTokenType.TimeSpan:
                    data.Type = "TimeSpan";
                    break;
                case JTokenType.Guid:
                    data.Type = "Guid";
                    break;
                case JTokenType.Uri:
                    data.Type = "Uri";
                    break;
                case JTokenType.Object:
                    data.Type = ToUpCamel(name);
                    break;
                default:
                    break;
            }
            return data;
        }

        private void MargeClassData()
        {
            var list = new List<ClassData>();
            foreach (var data in classDatas)
            {
                var find = list.Find(a => a.Path == data.Path);
                if (find == null)
                    list.Add(data);
                else
                {
                    foreach (var prop in data.Prop)
                    {
                        if (find.Prop.Exists(b => b.Name == prop.Name))
                            continue;
                        find.Prop.Add(prop);
                    }
                }
            }
            classDatas = list;
        }

        private void RenameClassData()
        {
            var list = new List<ClassData>();
            foreach (var data in classDatas)
            {
                list.Clear();
                var findAll = classDatas.FindAll(a => a.Name == data.Name);
                foreach (var find in findAll)
                {
                    if (list.Exists(a => a.Path == find.Path))
                        continue;
                    list.Add(find);
                }
                if (list.Count <= 1)
                    continue;
                for (var i = 0; i < list.Count; i++)
                {
                    var last = list[i].Path.LastIndexOf(".");
                    var path = list[i].Path.Substring(0, last);
                    var find = classDatas.Find(a => a.Path == path);
                    if (find != null)
                    {
                        foreach (var prop in find.Prop)
                        {
                            if (prop.Type != list[i].Name)
                                continue;
                            prop.Type = $"{list[i].Name}{i + 1}";
                        }
                    }
                    list[i].Name = $"{list[i].Name}{i + 1}";
                }
            }
        }

        private string CreateClass()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < classDatas.Count; i++)
            {
                if (i > 0)
                {
                    sb.AppendLine();
                    sb.AppendLine();
                }
                //sb.AppendLine($"[{classDatas[i].Path}]");
                sb.AppendLine($"public class {classDatas[i].Name}");
                sb.AppendLine("{");
                foreach (var prop in classDatas[i].Prop)
                {
                    sb.AppendLine($"    public {prop.Type}{prop.Array} {prop.Name} {{ get; set; }}");
                }
                sb.Append("}");
            }
            return sb.ToString();
        }
    }
}
